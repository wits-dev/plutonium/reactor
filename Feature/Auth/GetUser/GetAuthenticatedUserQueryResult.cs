using FluentResults;
using Plutonium.Reactor.Services.Auth.User;

namespace Plutonium.Reactor.Features.User.Fetch
{
  public class GetAuthenticatedUserQueryResult
  {
    public AuthenticatedUser User { get; internal set; }

    public GetAuthenticatedUserQueryResult()
    {
    }
  }
}

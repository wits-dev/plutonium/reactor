using Enexure.MicroBus;
using FluentResults;
using Plutonium.Reactor.Services.Auth.User;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Features.User.Fetch
{
  internal class GetAuthenticatedUserQueryHandler : IQueryHandler<GetAuthenticatedUserQuery, Result<GetAuthenticatedUserQueryResult>>
  {
    private readonly IAuthenticatedUserProvider _userProvider;

    public GetAuthenticatedUserQueryHandler(IAuthenticatedUserProvider userProvider)
    {
      _userProvider = userProvider;
    }

    public Task<Result<GetAuthenticatedUserQueryResult>> Handle(GetAuthenticatedUserQuery query)
    {
      var user = _userProvider.GetUser();

      return Task.FromResult(Results.Ok<GetAuthenticatedUserQueryResult>()
                      .WithSuccess("Retrieved authenticated user successfully")
                      .WithValue(new GetAuthenticatedUserQueryResult()
                      {
                        User = user,
                      }));
    }
  }
}

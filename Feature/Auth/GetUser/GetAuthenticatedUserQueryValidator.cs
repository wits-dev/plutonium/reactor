using FluentValidation;

namespace Plutonium.Reactor.Features.User.Fetch
{
    internal class GetAuthenticatedUserQueryValidator : AbstractValidator<GetAuthenticatedUserQuery>
    {
        public GetAuthenticatedUserQueryValidator()
        {
        }
    }
}
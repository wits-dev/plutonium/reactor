using Enexure.MicroBus;
using FluentResults;
using Plutonium.Reactor.Attributes;
using Plutonium.Reactor.Pipeline.Crosscutting.Auth;

namespace Plutonium.Reactor.Features.User.Fetch
{
  [AuthorizedMessage("*")]
  public class GetAuthenticatedUserQuery : IQuery<GetAuthenticatedUserQuery, Result<GetAuthenticatedUserQueryResult>>
  {
    public GetAuthenticatedUserQuery()
    {
    }
  }
}

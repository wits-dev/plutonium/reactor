﻿using System.Data;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Data
{
    /// <summary>
    /// The connection resolver allows us to reference the same connection througout the scope of the request
    /// This allows us to use the same connection and satifying the requirements of using the LTM of TransactionScope
    /// as DTC is not supported in net Reactor yet
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IConnectionResolver<T> where T : IDbConnection
    {
        Task<T> Resolve(string connectionString);
    }
}
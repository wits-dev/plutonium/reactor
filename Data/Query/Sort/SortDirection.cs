﻿namespace Plutonium.Reactor.Data.Query.Sort
{
    public enum SortDirection
    {
        Desc = 0,
        Asc = 1,
    }
}
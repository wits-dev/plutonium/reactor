﻿using System;

namespace Plutonium.Reactor.Data.Query.Sort
{
    public class SortBy<T> where T : struct, IConvertible
    {
        public T Column { get; set; }
        public SortDirection Direction { get; set; }

        public SortBy()
        {
        }

        public SortBy(T column, SortDirection order)
        {
            Column = column;
            Direction = order;
        }
    }
}
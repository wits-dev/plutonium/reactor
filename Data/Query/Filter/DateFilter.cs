﻿using System;

namespace Plutonium.Reactor.Data.Query.Filter
{
    public class DateFilter
    {
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
        public bool IncludeEndDate { get; set; }

        public DateFilter()
        {
        }

        public DateFilter(DateTimeOffset? start, DateTimeOffset? end, bool includeEndDate = false)
        {
            Start = start;
            End = end;
            IncludeEndDate = includeEndDate;
        }
    }
}
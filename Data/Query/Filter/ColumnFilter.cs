﻿using System;
using System.Linq;

namespace Plutonium.Reactor.Data.Query.Filter
{
    public class ColumnFilter<T> where T : struct, IConvertible
    {
        public T Column { get; set; }

        private object _value;

        public object Value
        {
            get
            {
                switch (_operator)
                {
                    case "startsWith":
                        return $"{_value}%";

                    case "endsWith":
                        return $"%{_value}";

                    case "contains":
                        return $"%{_value}%";

                    default:
                        return _value;
                }
            }
            set { _value = value; }
        }

        private string _operator;

        public string Operator
        {
            get
            {
                switch (_operator)
                {
                    case "eq":
                        return "=";

                    case "neq":
                        return "<>";

                    case "gt":
                        return ">";

                    case "gte":
                        return ">=";

                    case "lt":
                        return "<";

                    case "lte":
                        return "<=";

                    case "is":
                        return "IS";

                    case "isNot":
                        return "IS NOT";

                    case "startsWith":
                    case "endsWith":
                    case "contains":
                        return "LIKE";

                    default:
                        return "=";
                }
            }

            set { _operator = value; }
        }

        public ColumnFilter()
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }
        }

        public ColumnFilter(T column, object value, string @operator = "=")
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            Column = column;
            Operator = @operator;
            Value = value;
        }
    }
}
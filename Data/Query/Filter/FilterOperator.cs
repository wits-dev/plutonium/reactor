﻿using System;
using System.Linq;

namespace Plutonium.Reactor.Data.Query.Filter
{
    public enum FilterOperator
    {
        eq = 0,
        neq,
        gt,
        gte,
        lt,
        lte,
        @is,
        isNot,
        startsWith,
        endsWith,
        contains,
    }
}
﻿using FluentResults;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Pipeline
{
    public interface ICommandWithResult
    {
        TaskCompletionSource<Result> Result { get; }
    }
}
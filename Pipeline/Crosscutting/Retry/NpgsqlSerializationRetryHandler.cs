﻿using Enexure.MicroBus;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Npgsql;
using Polly;
using System;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Pipeline.Crosscutting.Retry
{
    internal class NpgsqlSerializationRetryHandler : IDelegatingHandler
    {
        private readonly ILogger<NpgsqlSerializationRetryHandler> _logger;

        public NpgsqlSerializationRetryHandler(ILogger<NpgsqlSerializationRetryHandler> logger)
        {
            _logger = logger;
        }

        public Task<object> Handle(INextHandler next, object message)
        {
            var msg = new { Type = message.GetType(), Data = JsonConvert.SerializeObject(message) };
            _logger.LogDebug("Message {message} dispatched", msg);

            return Policy
                .Handle<PostgresException>(r => r.SqlState == "40001") // serialization error
                .WaitAndRetryAsync(
                    10,
                    (retryAttempt) =>
                    {
                        var delay = Math.Pow(retryAttempt, 2) + (new Random().NextDouble() * 100);
                        return TimeSpan.FromMilliseconds(delay);
                    },
                    (exception, timeSpan, retryCount, context) =>
                    {
                        _logger.LogDebug("Retrying {message} on serialization error: {retryCount} {timeSpan}", msg, retryCount, timeSpan.Milliseconds);
                    })
                .ExecuteAsync(() => next.Handle(message));
        }
    }
}
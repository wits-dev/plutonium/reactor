﻿using Enexure.MicroBus;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Pipeline.Crosscutting.Logging
{
    internal class LoggingHandler : IDelegatingHandler
    {
        private readonly ILogger<LoggingHandler> _logger;

        public LoggingHandler(ILogger<LoggingHandler> logger)
        {
            _logger = logger;
        }

        public async Task<object> Handle(INextHandler next, object message)
        {
            var msg = new { Type = message.GetType(), Data = JsonConvert.SerializeObject(message) };
            _logger.LogDebug("Message {message} dispatched", msg);

            try
            {
                var result = await next.Handle(message).ConfigureAwait(false);

                var res = new { Type = result.GetType(), Data = JsonConvert.SerializeObject(result) };
                _logger.LogDebug("Message {message} handled: {result}", msg, res);

                return result;
            }
            catch (Exception e)
            {
                _logger.LogCritical(new EventId(0), e, "Message {message} failed: {errorMessage}", msg, e.Message);
                throw;
            }
        }
    }
}
﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Plutonium.Reactor.Services.Auth.Token;
using Plutonium.Reactor.Services.Auth.User;

namespace Plutonium.Reactor.Pipeline.Crosscutting.Auth
{
    internal class AspNetCoreBearerAuthorizationHandler : BaseAuthorizationHandler
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AspNetCoreBearerAuthorizationHandler(IAuthenticatedUserProvider userProvider, IHttpContextAccessor httpContextAccessor, IAuthTokenDecoder tokenDecoder, ILogger<AspNetCoreBearerAuthorizationHandler> logger) : base(userProvider, tokenDecoder, logger)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        protected override string GetToken()
        {
            var header = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            var bearer = header.ToString();

            return bearer.Replace("Bearer", "").Trim();
        }
    }
}
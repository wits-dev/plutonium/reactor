﻿using Enexure.MicroBus;
using Microsoft.Extensions.Logging;
using Plutonium.Reactor.Services.Auth.Token;
using Plutonium.Reactor.Services.Auth.User;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Pipeline.Crosscutting.Auth
{
    internal class BackgroundServiceAuthorizationHandler : IDelegatingHandler
    {
        private readonly AuthorizedUserProviderFromAuthToken _userProvider;

        public BackgroundServiceAuthorizationHandler(IAuthenticatedUserProvider userProvider, ILogger<AspNetCoreBearerAuthorizationHandler> logger)
        {
            _userProvider = (AuthorizedUserProviderFromAuthToken)userProvider;
        }

        public Task<object> Handle(INextHandler next, object message)
        {
            //all background calls use the system account
            _userProvider.UseAuthToken(new AuthToken()
            {
                Uid = 1,
                Tid = 1,
                Sub = "system",
                Name = "System",
                Roles = new string[] { "superuser" },
                Permissions = new string[] { "*" }
            });

            return next.Handle(message);
        }
    }
}
﻿namespace Plutonium.Reactor.Pipeline.Crosscutting.Auth.Exception
{
    public class UnAuthenticatedUserAgentException : System.Exception
    {
        public UnAuthenticatedUserAgentException() : base("User-agent is not authenticated")
        {
        }

        protected UnAuthenticatedUserAgentException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }

        public UnAuthenticatedUserAgentException(string message) : base(message)
        {
        }

        public UnAuthenticatedUserAgentException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
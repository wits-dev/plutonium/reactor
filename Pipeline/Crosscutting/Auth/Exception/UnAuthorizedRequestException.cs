﻿namespace Plutonium.Reactor.Pipeline.Crosscutting.Auth.Exception
{
    public class UnAuthorizedRequestException : System.Exception
    {
        public UnAuthorizedRequestException() : base("User-agent is not authorized to perform this action")
        {
        }

        protected UnAuthorizedRequestException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }

        public UnAuthorizedRequestException(string message) : base(message)
        {
        }

        public UnAuthorizedRequestException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
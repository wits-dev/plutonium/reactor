﻿using Enexure.MicroBus;
using Microsoft.Extensions.Logging;
using Plutonium.Reactor.Attributes;
using Plutonium.Reactor.Pipeline.Crosscutting.Auth.Exception;
using Plutonium.Reactor.Services.Auth.Token;
using Plutonium.Reactor.Services.Auth.User;
using System.Linq;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Pipeline.Crosscutting.Auth
{
    internal abstract class BaseAuthorizationHandler : IDelegatingHandler
    {
        private readonly AuthorizedUserProviderFromAuthToken _userProvider;
        private readonly IAuthTokenDecoder _tokenDecoder;
        private readonly ILogger _logger;

        public BaseAuthorizationHandler(IAuthenticatedUserProvider userProvider, IAuthTokenDecoder tokenDecoder, ILogger<AspNetCoreBearerAuthorizationHandler> logger)
        {
            _userProvider = (AuthorizedUserProviderFromAuthToken)userProvider;
            _tokenDecoder = tokenDecoder;
            _logger = logger;
        }

        public Task<object> Handle(INextHandler next, object message)
        {
            var authorization = (AuthorizedMessageAttribute)message
                .GetType()
                .GetCustomAttributes(false)
                .FirstOrDefault(attr => attr is AuthorizedMessageAttribute);

            if (authorization is null)
            {
                throw new System.Exception("Authorization attribute is missing");
            }

            _userProvider.UseAuthToken(null); //initialize the token first

            var strToken = GetToken();
            if (!string.IsNullOrWhiteSpace(strToken))
            {
                var token = _tokenDecoder.DecodeToken(strToken);
                if (token?.IsExpired == false)
                    _userProvider.UseAuthToken(token);
            }

            var user = _userProvider.GetUser();

            if (authorization.Permissions.Length == 1 && authorization.Permissions[0] == "*")
            {
                //all users allowed
            }
            else if (authorization.Permissions.Length == 1 && authorization.Permissions[0] == "?")
            {
                //only anonymous users allowed

                if (!user.IsAnonymous)
                {
                    throw new UnAuthorizedRequestException();
                }
            }
            else if (authorization.Permissions.Length == 1 && authorization.Permissions[0] == "!")
            {
                //any authenticated users allowed

                if (!user.IsAuthenticated)
                {
                    throw new UnAuthenticatedUserAgentException();
                }
            }
            else
            {
                //only authenticated users with required permissions allowed

                if (!user.IsAuthenticated)
                {
                    throw new UnAuthenticatedUserAgentException();
                }

                if (!user.HasPermissions(authorization.Permissions))
                {
                    throw new UnAuthorizedRequestException();
                }
            }

            return next.Handle(message);
        }

        protected abstract string GetToken();
    }
}
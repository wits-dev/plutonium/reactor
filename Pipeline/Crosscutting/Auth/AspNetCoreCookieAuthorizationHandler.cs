﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Plutonium.Reactor.Services.Auth.Token;
using Plutonium.Reactor.Services.Auth.User;

namespace Plutonium.Reactor.Pipeline.Crosscutting.Auth
{
    internal class AspNetCoreCookieAuthorizationHandler : BaseAuthorizationHandler
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AspNetCoreCookieAuthorizationHandler(IAuthenticatedUserProvider userProvider, IHttpContextAccessor httpContextAccessor, IAuthTokenDecoder tokenDecoder, ILogger<AspNetCoreBearerAuthorizationHandler> logger) : base(userProvider, tokenDecoder, logger)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        protected override string GetToken()
        {
            var cookie = _httpContextAccessor.HttpContext.Request.Cookies["Authorization"];

            return cookie;
        }
    }
}
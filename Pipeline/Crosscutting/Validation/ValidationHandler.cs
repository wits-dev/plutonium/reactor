﻿using Enexure.MicroBus;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Pipeline.Crosscutting.Validation
{
    internal class ValidationHandler : IDelegatingHandler
    {
        private readonly IServiceProvider _serviceProvider;

        public ValidationHandler(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Task<object> Handle(INextHandler next, object message)
        {
            var msgType = message.GetType();
            var validatorType = typeof(IValidator<>).MakeGenericType(msgType);
            var requiredType = typeof(IEnumerable<>).MakeGenericType(validatorType);

            var validators = (IEnumerable<IValidator>)_serviceProvider.GetRequiredService(requiredType);
            if (!validators.Any()) throw new Exception($"No validators found for {msgType}");

            var context = new ValidationContext(message);
            var failures = validators
                .Select(v => v.Validate(context))
                .SelectMany(result => result.Errors)
                .Where(f => f != null)
                .ToList();

            if (failures.Any())
                throw new ValidationException(failures);

            return next.Handle(message);
        }
    }
}
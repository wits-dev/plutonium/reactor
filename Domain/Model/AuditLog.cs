﻿using System;

namespace Plutonium.Reactor.Domain.Model
{
    public class AuditLog
    {
        public long Id { get; set; }
        public int UserId { get; set; }
        public int Username { get; set; }
        public string ActionName { get; set; }
        public string Description { get; set; }
        public string ObjectName { get; set; }
        public string ObjectData { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
    }
}
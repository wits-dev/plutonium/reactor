﻿using Microsoft.Extensions.Options;
using Npgsql;
using Plutonium.Reactor.Data;
using Plutonium.Reactor.Options;
using System;
using System.Data;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Repository
{
    public abstract class BasePgRepository
    {
        private readonly ConnectionStringOptions _connectionString;
        private readonly IConnectionResolver<NpgsqlConnection> _resolver;

        protected BasePgRepository(IOptions<ConnectionStringOptions> options, IConnectionResolver<NpgsqlConnection> resolver)
        {
            _connectionString = options.Value;
            _resolver = resolver;
        }

        private async Task<IDbConnection> GetConnection()
        {
            return await _resolver.Resolve(_connectionString.Value).ConfigureAwait(false); ;
        }

        protected async Task<T> WithConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            var connection = await GetConnection().ConfigureAwait(false);

            // Asynchronously execute getData, which has been passed in as a Func<IDBConnection, Task<T>>
            return await getData(connection).ConfigureAwait(false);
        }

        protected async Task WithConnection(Func<IDbConnection, Task> getData)
        {
            var connection = await GetConnection().ConfigureAwait(false);

            // Asynchronously execute getData, which has been passed in as a Func<IDBConnection, Task<T>>
            await getData(connection).ConfigureAwait(false);
        }
    }
}
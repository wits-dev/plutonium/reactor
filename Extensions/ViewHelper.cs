﻿using Microsoft.AspNetCore.Http;
using System.Linq;

namespace Plutonium.Reactor.Extensions
{
    public static class ViewHelper
    {
        public static string Merge(this IQueryCollection query, params (object, object)[] parameters)
        {
            var newQuery = query.ToDictionary(q => q.Key, q => q.Value);
            foreach (var param in parameters)
            {
                newQuery[param.Item1.ToString()] = param.Item2.ToString();
            }

            return $"?{string.Join("&", newQuery.Select(q => $"{q.Key}={q.Value}"))}";
        }
    }
}
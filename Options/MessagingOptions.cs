﻿namespace Plutonium.Reactor.Options
{
    public class MessagingOptions
    {
        public string EmailApiKey { get; set; }
        public string EmailFromName { get; set; }
        public string EmailFromAddress { get; set; }
    }
}
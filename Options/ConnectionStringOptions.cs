﻿namespace Plutonium.Reactor.Options
{
    public class ConnectionStringOptions
    {
        public string Value { get; set; }
    }
}
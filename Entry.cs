﻿using Autofac;
using Enexure.MicroBus;
using Microsoft.Extensions.Hosting;
using Plutonium.Reactor.Data;
using Plutonium.Reactor.Pipeline.Crosscutting.Auth;
using Plutonium.Reactor.Pipeline.Crosscutting.Logging;
using Plutonium.Reactor.Pipeline.Crosscutting.Retry;
using Plutonium.Reactor.Pipeline.Crosscutting.Validation;
using Plutonium.Reactor.Services.Auth.User;
using Plutonium.Reactor.Services.Hash;
using Plutonium.Reactor.Services.MenuProvider;
using Plutonium.Reactor.Services.Messaging.Email.Provider;
using Plutonium.Reactor.Services.PatternMatcher.RegexPattern;
using Plutonium.Reactor.Services.PDF;
using Plutonium.Reactor.Services.TemplateRenderer;
using System;
using WITS.Cache;

namespace Plutonium.Reactor
{
    public static class Entry
    {
        public static BusBuilder RegisterReactor(this BusBuilder builder, HostAuthType appType)
        {
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;

            builder
                .RegisterGlobalHandler<LoggingHandler>()
                .RegisterGlobalHandler<ValidationHandler>()
                .RegisterGlobalHandler<NpgsqlSerializationRetryHandler>()
            ;

            switch (appType)
            {
                case HostAuthType.Asp:
                    builder.RegisterGlobalHandler<AspNetCoreAuthorizationHandler>();
                    break;

                case HostAuthType.AspBearerOnly:
                    builder.RegisterGlobalHandler<AspNetCoreBearerAuthorizationHandler>();
                    break;

                case HostAuthType.AspCookieOnly:
                    builder.RegisterGlobalHandler<AspNetCoreCookieAuthorizationHandler>();
                    break;

                case HostAuthType.Service:
                    builder.RegisterGlobalHandler<BackgroundServiceAuthorizationHandler>();
                    break;

                case HostAuthType.None:
                    // do nothing
                    break;

                default:
                    throw new NotImplementedException($"Unknown host type: '{Convert.ToString(appType)}'");
            }

            builder.RegisterHandlers(typeof(Entry).Assembly);

            return builder;
        }

        public class ReactorModule : Module
        {
            private bool _useNodePDFGenerator;
            private bool _useHandleBarsRenderer;
            private bool _useMemoryCache;
            private MailProvider _mailProvider;

            public ReactorModule UseNodePDFGenerator()
            {
                _useNodePDFGenerator = true;
                return this;
            }

            public ReactorModule UseHandleBarsRenderer()
            {
                _useHandleBarsRenderer = true;
                return this;
            }

            public ReactorModule UseMemoryCache()
            {
                _useMemoryCache = true;
                return this;
            }

            public ReactorModule UseSendGrid()
            {
                _mailProvider = MailProvider.SendGrid;
                return this;
            }

            protected override void Load(ContainerBuilder builder)
            {
                builder
                    .RegisterAssemblyTypes(typeof(Entry).Assembly)
                    .Where(type => !type.IsAssignableTo<IHostedService>())
                    .AsImplementedInterfaces();

                builder
                    .RegisterType<AuthorizedUserProviderFromAuthToken>()
                    .AsImplementedInterfaces()
                    .InstancePerLifetimeScope();

                builder
                    .RegisterType<Hasher>()
                    .AsImplementedInterfaces()
                    .SingleInstance();

                builder
                    .RegisterType<RegexPatternMatcher>()
                    .AsImplementedInterfaces()
                    .SingleInstance();

                builder
                    .RegisterType<PgConnectionResolver>()
                    .AsImplementedInterfaces()
                    .InstancePerLifetimeScope();

                builder
                    .RegisterType<MenuProvider>()
                    .AsImplementedInterfaces()
                    .SingleInstance();

                if (_useNodePDFGenerator)
                {
                    builder
                        .RegisterType<NodePDFGenerator>()
                        .AsImplementedInterfaces()
                        .InstancePerLifetimeScope();
                }

                if (_useHandleBarsRenderer)
                {
                    builder
                        .RegisterType<HandleBarsTemplateRenderer>()
                        .AsImplementedInterfaces()
                        .InstancePerLifetimeScope();
                }

                if (_useMemoryCache)
                {
                    builder
                        .RegisterType<MemoryCacheManager>()
                        .AsImplementedInterfaces()
                        .SingleInstance();
                }

                switch (_mailProvider)
                {
                    case MailProvider.None:
                        //do nothing
                        break;

                    case MailProvider.SendGrid:
                        builder
                            .RegisterType<SendGridProvider>()
                            .AsImplementedInterfaces()
                            .SingleInstance();
                        break;

                    default:
                        throw new NotImplementedException($"MailProvider: {_mailProvider}");
                }
            }
        }

        public enum HostAuthType
        {
            None,
            Asp,
            AspBearerOnly,
            AspCookieOnly,
            Service
        }

        public enum MailProvider
        {
            None = 0,
            SendGrid,
        }
    }
}
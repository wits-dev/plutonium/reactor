﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Plutonium.Reactor.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
    public class MenuItemAttribute : Attribute
    {
        public string Name { get; }
        public string Controller { get; }
        public string Route { get; }
        public int Position { get; }
        public string Icon { get; }
        public string Group { get; }
        public string[] Roles { get; }
        public string[] Permissions { get; }

        public MenuItemAttribute(
            string name,
            string controller,
            string route,
            int position = 999,
            string icon = "apps",
            string group = null,
            string[] permissions = null,
            string[] roles = null)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("cannot be null or empty", nameof(name));
            }

            if (string.IsNullOrWhiteSpace(controller))
            {
                throw new ArgumentException("cannot be null or empty", nameof(controller));
            }

            if (string.IsNullOrWhiteSpace(route))
            {
                throw new ArgumentException("cannot be null or empty", nameof(route));
            }

            Name = name;
            Controller = controller;
            Route = route;
            Position = position;
            Icon = icon;
            Group = group;
            Permissions = permissions;
            Roles = roles;
        }
    }
}
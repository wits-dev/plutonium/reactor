﻿using System;

namespace Plutonium.Reactor.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class AuthorizedMessageAttribute : Attribute
    {
        public string[] Permissions { get; }

        public AuthorizedMessageAttribute(params string[] permissions)
        {
            Permissions = permissions ?? throw new ArgumentNullException(nameof(permissions));
        }
    }
}
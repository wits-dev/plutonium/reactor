﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Plutonium.Reactor.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
    public class MenuGroupAttribute : Attribute
    {
        public string Name { get; }
        public int Position { get; }
        public string Icon { get; }
        public string Group { get; }
        public string[] Roles { get; }
        public string[] Permissions { get; }

        public MenuGroupAttribute(
            string name,
            int position = 999,
            string icon = "apps",
            string group = null,
            string[] permissions = null,
            string[] roles = null)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("cannot be null or empty", nameof(name));
            }

            Name = name;
            Position = position;
            Icon = icon;
            Group = group;
            Permissions = permissions;
            Roles = roles;
        }
    }
}
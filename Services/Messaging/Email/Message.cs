﻿using System;
using System.Collections.Generic;
using System.Text;
using Plutonium.Reactor.Options;

namespace Plutonium.Reactor.Services.Messaging.Email
{
    public struct Message
    {
        public string Subject { get; set; }
        public string Html { get; set; }
        public string Text { get; set; }

        public Message(string subject, string html, string text) : this()
        {
            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new ArgumentException("cannot be null or empty", nameof(subject));
            }

            if (string.IsNullOrWhiteSpace(html))
            {
                throw new ArgumentException("cannot be null or empty", nameof(html));
            }

            if (string.IsNullOrWhiteSpace(text))
            {
                throw new ArgumentException("cannot be null or empty", nameof(text));
            }

            Subject = subject;
            Html = html;
            Text = text;
        }

        public Message(string subject, string html) : this()
        {
            Subject = subject;
            Html = html;
        }
    }
}
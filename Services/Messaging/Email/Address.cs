﻿using System;
using System.Collections.Generic;
using System.Text;
using Plutonium.Reactor.Options;

namespace Plutonium.Reactor.Services.Messaging.Email
{
    public struct Address
    {
        public string Email { get; set; }
        public string Name { get; set; }

        public Address(string address, string name) : this()
        {
            if (string.IsNullOrWhiteSpace(address))
            {
                throw new ArgumentException("cannot be null or empty", nameof(address));
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("cannot be null or empty", nameof(name));
            }

            Email = address;
            Name = name;
        }

        public Address(string address) : this()
        {
            Email = address;
            Name = null;
        }
    }
}
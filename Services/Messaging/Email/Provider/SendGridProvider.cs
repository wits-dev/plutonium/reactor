﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Plutonium.Reactor.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Services.Messaging.Email.Provider
{
    internal class SendGridProvider : IEmailSender
    {
        protected static SendGridClient Client;

        protected readonly ILogger<SendGridProvider> _logger;
        protected readonly Options.MessagingOptions _messagingOptions;

        public SendGridProvider(
            ILogger<SendGridProvider> logger,
            IOptions<Options.MessagingOptions> messagingOptions)
        {
            _logger = logger;
            _messagingOptions = messagingOptions.Value;

            Client = Client ?? new SendGridClient(_messagingOptions.EmailApiKey);
        }

        public Task<Response> SendEmail(
            Message message,
            Address[] to,
            Address[] cc = null,
            Address[] bcc = null)
        {
            var from = string.IsNullOrWhiteSpace(_messagingOptions.EmailFromName)
                            ? new Address(_messagingOptions.EmailFromAddress)
                            : new Address(_messagingOptions.EmailFromAddress, _messagingOptions.EmailFromName);

            return SendEmail(from, message, to, cc, bcc);
        }

        public async Task<Response> SendEmail(
            Address from,
            Message message,
            Address[] to,
            Address[] cc = null,
            Address[] bcc = null)
        {
            if (to == null)
                throw new ArgumentNullException(nameof(to));

            if (to.Length == 0)
                throw new ArgumentException("cannot be empty", nameof(to));

            var subject = message.Subject;

            var textContent = message.Text;
            var htmlContent = message.Html;

            _logger.LogDebug("Sending email with {subject} to {recipients}", subject, to);

            var msg = new SendGridMessage()
            {
                From = new EmailAddress(from.Email, from.Name),
                Subject = subject,
                HtmlContent = htmlContent
            };

            if (!string.IsNullOrWhiteSpace(message.Text))
                msg.PlainTextContent = textContent;

            #region recipients

            var tos = to
                        .Select(x => new EmailAddress(x.Email, x.Name))
                        .ToList();
            msg.AddTos(tos);

            if (cc?.Length > 0)
            {
                var ccs = cc
                            .Select(x => new EmailAddress(x.Email, x.Name))
                            .ToList();
                msg.AddTos(ccs);
            }

            if (bcc?.Length > 0)
            {
                var bccs = bcc
                            .Select(x => new EmailAddress(x.Email, x.Name))
                            .ToList();
                msg.AddTos(bccs);
            }

            #endregion recipients

            var apiResponse = await Client.SendEmailAsync(msg).ConfigureAwait(false);

            var statusCode = apiResponse.StatusCode;
            var statusMessage = await apiResponse.Body.ReadAsStringAsync().ConfigureAwait(false);
            var response = new Response(statusCode, statusMessage);

            if (response.IsSuccess)
                _logger.LogInformation("Successfully sent email with {subject} to {recipients}. Response: {response}'", subject, to, response);
            else
                _logger.LogError("Failed to send email with {subject} to {recipients}. Response: {response}'", subject, to, response);

            return response;
        }
    }
}
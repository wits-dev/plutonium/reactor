﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Http;
using Plutonium.Reactor.Options;

namespace Plutonium.Reactor.Services.Messaging.Email
{
    public struct Response
    {
        public HttpStatusCode StatusCode { get; set; }
        public string StatusMessage { get; set; }

        public bool IsSuccess { get { return StatusCode == HttpStatusCode.OK || StatusCode == HttpStatusCode.Accepted; } }

        public Response(HttpStatusCode statusCode, string statusMessage)
        {
            StatusCode = statusCode;
            StatusMessage = statusMessage;
        }

        public override string ToString()
        {
            return $"{{ {StatusCode}:{StatusMessage} }}";
        }
    }
}
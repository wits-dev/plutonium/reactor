﻿using Plutonium.Reactor.Services.Messaging.Email;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Services.Messaging
{
    public interface IEmailSender
    {
        Task<Response> SendEmail(
            Message message,
            Address[] to,
            Address[] cc = null,
            Address[] bcc = null)
        ;

        Task<Response> SendEmail(
            Address from,
            Message message,
            Address[] to,
            Address[] cc = null,
            Address[] bcc = null);
    }
}
﻿using Plutonium.Reactor.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Plutonium.Reactor.Services.MenuProvider
{
    public class MenuItem
    {
        internal int Position { get; set; }

        public string Name { get; set; }
        public string Icon { get; set; }

        public string[] Roles { get; set; }
        public string[] Permissions { get; set; }

        public bool IsGroup { get; set; }

        public MenuItem[] Items { get; set; }

        public string Controller { get; set; }
        public string Route { get; set; }

        public string Id
        {
            get
            {
                return $"menu-{GenerateSlug(Name)}";
            }
        }

        protected static string GenerateSlug(string phrase)
        {
            string str = RemoveAccent(phrase ?? "").ToLower();

            // invalid chars
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");

            // convert multiple spaces into one space
            str = Regex.Replace(str, @"\s+", " ").Trim();

            return Regex.Replace(str, @"\s", "-");
        }

        protected static string RemoveAccent(string txt)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }
    }
}
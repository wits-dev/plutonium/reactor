﻿using Plutonium.Reactor.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Plutonium.Reactor.Services.MenuProvider
{
    internal class MenuProvider : IMenuProvider
    {
        public MenuItem[] GetMenu(Assembly assembly)
        {
            var menuGroups = assembly
                .GetCustomAttributes(typeof(MenuGroupAttribute), false)
                .Select(a => (MenuGroupAttribute)a)
                .ToArray();

            var menuItems = assembly
                .GetCustomAttributes(typeof(MenuItemAttribute), false)
                .Select(a => (MenuItemAttribute)a)
                .ToArray();

            var rootItems = menuItems
                .Where(x => string.IsNullOrWhiteSpace(x.Group))
                .ToArray();

            var nestedItems = menuItems
                .Where(x => !string.IsNullOrWhiteSpace(x.Group))
                .ToArray();

            return rootItems.Select(x => new MenuItem
            {
                Position = x.Position,

                Name = x.Name,
                Icon = x.Icon,
                Roles = x.Roles,
                Permissions = x.Permissions,

                IsGroup = false,
                Controller = x.Controller,
                Route = x.Route,
            })
            .Concat(menuGroups.Select(x => new MenuItem
            {
                Position = x.Position,

                Name = x.Name,
                Icon = x.Icon,
                Roles = x.Roles,
                Permissions = x.Permissions,

                IsGroup = true,
                Items = nestedItems
                            .Where(n => n.Group == x.Name)
                            .Select(n => new MenuItem
                            {
                                Position = n.Position,

                                Name = n.Name,
                                Icon = n.Icon,
                                Roles = n.Roles,
                                Permissions = n.Permissions,

                                IsGroup = false,
                                Controller = n.Controller,
                                Route = n.Route,
                            })
                            .OrderBy(n => n.Position)
                            .ToArray(),
            }))
            .OrderBy(x => x.Position)
            .ToArray();
        }
    }
}
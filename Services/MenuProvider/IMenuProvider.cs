﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Plutonium.Reactor.Services.MenuProvider
{
    public interface IMenuProvider
    {
        MenuItem[] GetMenu(Assembly assembly);
    }
}
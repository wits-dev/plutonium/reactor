﻿namespace Plutonium.Reactor.Services.Hash.Sha
{
    public interface ISha256Hasher
    {
        string Sha256(string input);
    }
}
﻿namespace Plutonium.Reactor.Services.Hash.Md5
{
    public interface IMd5Hasher
    {
        string Md5(string input);
    }
}
﻿namespace Plutonium.Reactor.Services.PatternMatcher.RegexPattern
{
    public interface IPatternMatcher
    {
        /// <summary>
        /// Try to match a given pattern. If pattern is matched, matching string is returned.
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        string[] TryMatch(string subject, string pattern, bool caseInsensitive = true);
    }
}
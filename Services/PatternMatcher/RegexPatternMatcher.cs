﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Plutonium.Reactor.Services.PatternMatcher.RegexPattern
{
    internal class RegexPatternMatcher : IPatternMatcher
    {
        public string[] TryMatch(string subject, string pattern, bool caseInsensitive = true)
        {
            var matches = new List<string>();

            var reg = new Regex(pattern, caseInsensitive ? RegexOptions.IgnoreCase : RegexOptions.None);

            foreach (var match in reg.Matches(subject))
            {
                matches.Add(match.ToString());
            }

            return matches.ToArray();
        }
    }
}
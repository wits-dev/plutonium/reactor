﻿using System.Threading.Tasks;

namespace Plutonium.Reactor.Services.PDF
{
    public interface IPDFGenerator
    {
        Task<byte[]> FromHtml(string html);
    }
}
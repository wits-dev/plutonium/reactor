﻿using Microsoft.AspNetCore.NodeServices;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Services.PDF
{
    internal class NodePDFGenerator : IPDFGenerator
    {
        private readonly INodeServices _nodeServices;

        public NodePDFGenerator(INodeServices nodeServices)
        {
            _nodeServices = nodeServices;
        }

        public Task<byte[]> FromHtml(string html)
        {
            return _nodeServices.InvokeAsync<byte[]>("./Resources/Node/Pdf", html);
        }
    }
}
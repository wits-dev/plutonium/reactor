﻿using System.Threading.Tasks;

namespace Plutonium.Reactor.Services.TemplateRenderer
{
    public interface ITemplateRenderer
    {
        Task<string> Render<TModel>(string templateKey, TModel model);
    }
}
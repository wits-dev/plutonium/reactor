﻿using HandlebarsDotNet;
using System.IO;
using System.Threading.Tasks;

namespace Plutonium.Reactor.Services.TemplateRenderer
{
    internal class HandleBarsTemplateRenderer : ITemplateRenderer
    {
        public Task<string> Render<TModel>(string templateKey, TModel model)
        {
            var path = Path.Combine("Resources", "Templates", templateKey + ".handlebars");
            if (!File.Exists(path))
                throw new FileNotFoundException(path);

            var source = File.ReadAllText(path);
            return Task.FromResult(Handlebars.Compile(source)(model));
        }
    }
}
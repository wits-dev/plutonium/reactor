﻿using System;
using System.Linq;

namespace Plutonium.Reactor.Services.Auth.User
{
    public class AuthenticatedUser
    {
        public int Id { get; }
        public int TenantId { get; }
        public string Username { get; }
        public string DisplayName { get; }
        public string[] Roles { get; }
        public string[] Permissions { get; }

        public bool IsAuthenticated { get { return Id > 0; } }
        public bool IsAnonymous { get { return Id <= 0; } }

        private readonly bool IsSuperAdmin;

        public AuthenticatedUser(
            int id,
            int tenantId,
            string username,
            string displayName,
            string[] roles,
            string[] permissions)
        {
            Id = id;
            TenantId = tenantId;
            Username = username ?? throw new ArgumentNullException(nameof(username));
            DisplayName = displayName;
            Roles = roles?.Select(x => x.ToLowerInvariant())?.ToArray() ?? throw new ArgumentNullException(nameof(roles));
            Permissions = permissions?.Select(x => x.ToLowerInvariant())?.ToArray() ?? throw new ArgumentNullException(nameof(permissions));

            IsSuperAdmin = Permissions.Contains("*");
        }

        public bool HasRoles(params string[] roles)
        {
            if (roles is null || roles.Length == 0)
                return true;

            return IsSuperAdmin || !roles.Select(x => x.ToLowerInvariant()).Except(Roles).Any();
        }

        public bool HasPermissions(params string[] permissions)
        {
            if (permissions is null || permissions.Length == 0)
                return true;

            return IsSuperAdmin || !permissions.Select(x => x.ToLowerInvariant()).Except(Permissions).Any();
        }

        public bool HasAnyRoles(params string[] roles)
        {
            if (roles is null || roles.Length == 0)
                return true;

            return IsSuperAdmin || Roles.Any(x => roles.Contains(x));
        }

        public bool HasAnyPermissions(params string[] permissions)
        {
            if (permissions is null || permissions.Length == 0)
                return true;

            return IsSuperAdmin || Permissions.Any(x => permissions.Contains(x));
        }
    }

    public class AnonymousUser : AuthenticatedUser
    {
        public AnonymousUser() : base(
            -1,
            -1,
            "guest",
            "Guest",
            new string[0],
            new string[0])
        {
        }
    }
}
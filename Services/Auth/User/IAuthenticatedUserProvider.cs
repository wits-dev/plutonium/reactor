﻿using Plutonium.Reactor.Services.Auth.Token;
using System;

namespace Plutonium.Reactor.Services.Auth.User
{
    public interface IAuthenticatedUserProvider
    {
        AuthenticatedUser GetUser();
    }

    internal class AuthorizedUserProviderFromAuthToken : IAuthenticatedUserProvider
    {
        private bool Initialized = false;
        private AuthenticatedUser User = null;

        internal void UseAuthToken(AuthToken token)
        {
            if (token != null)
            {
                User = new AuthenticatedUser(
                    token.Uid,
                    token.Tid,
                    token.Sub,
                    token.Name,
                    token.Roles,
                    token.Permissions);
            }
            else
            {
                User = new AnonymousUser();
            }

            Initialized = true;
        }

        public AuthenticatedUser GetUser()
        {
            if (!Initialized)
                throw new Exception("AuthToken not initialiazed!");

            return User;
        }
    }
}
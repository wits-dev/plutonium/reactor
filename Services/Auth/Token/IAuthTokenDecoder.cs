﻿namespace Plutonium.Reactor.Services.Auth.Token
{
    internal interface IAuthTokenDecoder
    {
        AuthToken DecodeToken(string token);
    }
}
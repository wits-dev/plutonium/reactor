﻿using System;

namespace Plutonium.Reactor.Services.Auth.Token
{
    // if you change the properties, remember to change IAuthTokenProvider as well
    internal class AuthToken
    {
        public string Jti { get; set; }
        public string Iss { get; set; }
        public int Uid { get; set; }
        public string Sub { get; set; }
        public long Iat { get; set; }
        public long Exp { get; set; }

        /// <summary>
        /// Tenant Id
        /// </summary>
        public int Tid { get; set; }

        /// <summary>
        /// Profile Name
        /// </summary>
        public string Name { get; set; }

        public string[] Roles { get; set; }
        public string[] Permissions { get; set; }

        private DateTimeOffset SubmittedAt = DateTimeOffset.Now;

        public bool IsExpired
        {
            get
            {
                return DateTimeOffset.FromUnixTimeSeconds(Exp) < SubmittedAt;
            }
        }
    }
}
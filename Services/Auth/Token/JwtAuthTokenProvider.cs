﻿using Jose;
using MassTransit;
using System;
using System.Collections.Generic;

namespace Plutonium.Reactor.Services.Auth.Token
{
    internal class JwtAuthTokenProvider : IAuthTokenProvider
    {
        public string GenerateToken(
            int id,
            string username,
            int tenantId,
            string displayName,
            string[] roles,
            string[] permissions,
            string issuer,
            int ttlMins)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentOutOfRangeException(nameof(username), $"{nameof(username)} cannot be null or empty");
            if (permissions == null)
                throw new ArgumentNullException(nameof(permissions));
            if (string.IsNullOrWhiteSpace(issuer))
                throw new ArgumentOutOfRangeException(nameof(issuer), $"{nameof(issuer)} cannot be null or empty");

            // todo: make audience and expiry configurable
            var payload = new Dictionary<string, object>()
            {
                { "jti", NewId.NextGuid().ToString() },
                { "iss", issuer.Trim().ToLower() },
                { "uid", id },
                { "sub", username.Trim().ToLower() },
                { "iat", DateTimeOffset.Now.ToUnixTimeSeconds() },
                { "exp", DateTimeOffset.Now.AddMinutes(ttlMins).ToUnixTimeSeconds() },
                { "tid", tenantId },
                { "name", displayName },
                { "roles", roles },
                { "permissions", permissions },
            };

            return JWT.Encode(payload, null, JwsAlgorithm.none);
        }
    }
}
﻿namespace Plutonium.Reactor.Services.Auth.Token
{
    public interface IAuthTokenProvider
    {
        string GenerateToken(
            int id,
            string username,
            int tenantId,
            string displayName,
            string[] roles,
            string[] permissions,
            string issuer,
            int ttlMins);
    }
}
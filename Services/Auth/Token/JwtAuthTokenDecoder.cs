﻿using Jose;
using Microsoft.Extensions.Logging;
using System;

namespace Plutonium.Reactor.Services.Auth.Token
{
    internal class JwtAuthTokenDecoder : IAuthTokenDecoder
    {
        private readonly ILogger<JwtAuthTokenDecoder> _logger;

        public JwtAuthTokenDecoder(ILogger<JwtAuthTokenDecoder> logger)
        {
            _logger = logger;
        }

        public AuthToken DecodeToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentOutOfRangeException(nameof(token), $"{nameof(token)} cannot be null or empty");

            try
            {
                return JWT.Decode<AuthToken>(token);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "Failed to decode token '{token}' with error '{message}'", token, e.Message);

                return null;
            }
        }
    }
}
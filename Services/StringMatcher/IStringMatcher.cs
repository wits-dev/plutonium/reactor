﻿namespace Plutonium.Reactor.Services.LTest
{
    public interface IStringMatcher
    {
        bool FuzzyMatches(string target, string source, int fullDistance = 3, int partsDistance = 2);
    }
}
﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Plutonium.Reactor.Services.LTest
{
    internal class LTestStringMatcher : IStringMatcher
    {
        public bool FuzzyMatches(string target, string source, int fullDistance = 3, int partsDistance = 2)
        {
            target = Regex.Replace(target.ToLowerInvariant(), "[^a-zA-Z0-9]", " ");
            source = Regex.Replace(source.ToLowerInvariant(), "[^a-zA-Z0-9]", " ");

            int distance = ComputeDistance(target, source);

            if (distance <= fullDistance) return true;

            var targets = target.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var sources = source.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var requiredMatches = targets.Count();
            var matches = 0;
            foreach (var n in targets)
            {
                var matched = false;
                foreach (var y in sources)
                {
                    matched = ComputeDistance(n, y) <= partsDistance;

                    if (matched)
                    {
                        matches++;
                        break;
                    }
                }
            }

            return requiredMatches > 2 ? matches >= (requiredMatches - 1) : matches == requiredMatches;
        }

        /// <summary>
        /// Compute the distance between two strings.
        /// </summary>
        private static int ComputeDistance(string s, string t)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }
    }
}